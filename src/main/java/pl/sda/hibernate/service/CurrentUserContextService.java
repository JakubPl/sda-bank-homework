package pl.sda.hibernate.service;

public interface CurrentUserContextService {

    LoggedInUserHolder getLoggedInUserHolder();

    LoggedInUserHolder login(String pesel, String password);
    LoggedInUserHolder logout();
}
