package pl.sda.hibernate.service.impl;

import pl.sda.hibernate.dao.HistoryDao;
import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.dto.HistoryRecord;
import pl.sda.hibernate.entity.HistoryEntity;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.entity.enums.OperationType;
import pl.sda.hibernate.service.LoggedInUserHolder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class HistoryServiceImpl implements pl.sda.hibernate.service.HistoryService {
    private static final Set<OperationType> ALL_OPERATION_TYPES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(OperationType.values())));
    private LoggedInUserHolder loggedInUserHolder;
    private final HistoryDao historyDao;
    private final UserDao userDao;

    public HistoryServiceImpl(LoggedInUserHolder loggedInUserHolder, HistoryDao  historyDao, UserDao userDao) {
        this.loggedInUserHolder = loggedInUserHolder;
        this.historyDao = historyDao;
        this.userDao = userDao;
    }

    @Override
    public void registerOperation(OperationType operationType, BigDecimal balanceChange) {
        final UserEntity currentUser = userDao.find(loggedInUserHolder.getLoggedInUserId());

        HistoryEntity historyEntity = new HistoryEntity();
        historyEntity.setChange(balanceChange);
        historyEntity.setOperationType(operationType);
        historyEntity.setCreated(LocalDateTime.now());
        historyEntity.setOwner(currentUser);
        historyDao.save(historyEntity);
    }

    @Override
    public List<HistoryRecord> listUserActions() {
        return historyDao.findByUser(loggedInUserHolder.getLoggedInUserId(), ALL_OPERATION_TYPES).stream()
                .map(this::mapToHistoryRecord)
                .collect(Collectors.toList());
    }

    @Override
    public List<HistoryRecord> listUserTransactions() {
        return historyDao.findByUser(loggedInUserHolder.getLoggedInUserId(), OperationType.TRANSFERS).stream()
                .map(this::mapToHistoryRecord)
                .collect(Collectors.toList());
    }

    @Override
    public HistoryServiceImpl asUser(UserEntity userEntity) {
        return new HistoryServiceImpl(LoggedInUserHolder.asUser(userEntity),  historyDao, userDao);
    }

    private HistoryRecord mapToHistoryRecord(HistoryEntity e) {
        return new HistoryRecord(e.getOperationType(), e.getChange());
    }
}
