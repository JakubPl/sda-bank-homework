package pl.sda.hibernate.service.impl;

import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.service.CurrentUserContextService;
import pl.sda.hibernate.service.LoggedInUserHolder;

public class CurrentUserContextServiceImpl implements CurrentUserContextService {
    private final UserDao userDao;
    private final LoggedInUserHolder loggedInUserHolder;

    public CurrentUserContextServiceImpl(UserDao userDao, LoggedInUserHolder loggedInUserHolder) {
        this.userDao = userDao;
        this.loggedInUserHolder = loggedInUserHolder;
    }

    @Override
    public LoggedInUserHolder getLoggedInUserHolder() {
        return loggedInUserHolder;
    }

    @Override
    public LoggedInUserHolder login(String pesel, String password) {
        if(loggedInUserHolder != LoggedInUserHolder.NO_USER) {
            throw new IllegalArgumentException("There is already logged in user");
        }
        final UserEntity loggedInUser = userDao.findByPeselAndPassword(pesel, password);
        final String surname = loggedInUser.getSurname();
        final String name = loggedInUser.getName();
        return LoggedInUserHolder.asUser(loggedInUser);
    }

    @Override
    public LoggedInUserHolder logout() {
        return LoggedInUserHolder.NO_USER;
    }
}
