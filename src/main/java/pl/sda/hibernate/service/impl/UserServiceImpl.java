package pl.sda.hibernate.service.impl;

import pl.sda.hibernate.dao.AccountDao;
import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.dto.User;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.service.HistoryService;
import pl.sda.hibernate.service.LoggedInUserHolder;

public class UserServiceImpl implements pl.sda.hibernate.service.UserService {
    private final HistoryService historyService;
    private UserDao userDao;
    private final AccountDao accountDao;

    public UserServiceImpl(HistoryService historyService, UserDao userDao, AccountDao accountDao) {
        this.historyService = historyService;
        this.userDao = userDao;
        this.accountDao = accountDao;
    }

    public UserEntity register(User user) {
        if (validate(user)) {
            final UserEntity userEntity = new UserEntity();

            userEntity.setName(user.getName());
            userEntity.setSurname(user.getSurname());
            userEntity.setPassword(user.getPassword());
            userEntity.setPesel(user.getPesel());

            this.userDao.save(userEntity);


            new AccountServiceImpl(LoggedInUserHolder.actOnBehalfOf(userEntity.getId()), historyService.asUser(userEntity), userDao, accountDao).registerNormal();

            return userEntity;
        } else {
            //TODO: that should be seperate exception
            throw new RuntimeException("Invalid user data");
        }

    }

    private boolean validate(User user) {
        final boolean peselLengthValid = user.getPesel().length() == 11;
        final boolean passwordLenghtValid = user.getPassword().length() > 7;
        final boolean passwordRepeated = user.getPassword() != null && user.getPassword().equals(user.getRepeatPassword());
        return passwordRepeated && peselLengthValid && passwordLenghtValid;
    }
}
