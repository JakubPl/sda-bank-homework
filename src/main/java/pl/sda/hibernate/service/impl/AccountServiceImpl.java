package pl.sda.hibernate.service.impl;

import pl.sda.hibernate.dao.AccountDao;
import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.dto.Account;
import pl.sda.hibernate.entity.AccountEntity;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.entity.enums.AccountType;
import pl.sda.hibernate.entity.enums.OperationType;
import pl.sda.hibernate.service.AccountService;
import pl.sda.hibernate.service.HistoryService;
import pl.sda.hibernate.service.LoggedInUserHolder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class AccountServiceImpl implements AccountService {
    private final HistoryService historyService;
    private final UserDao userDao;
    private final AccountDao accountDao;
    private final LoggedInUserHolder loggedInUserHolder;

    public AccountServiceImpl(LoggedInUserHolder loggedInUserHolder, HistoryService historyService, UserDao userDao, AccountDao accountDao) {
        this.historyService = historyService;
        this.userDao = userDao;
        this.accountDao = accountDao;
        this.loggedInUserHolder = loggedInUserHolder;
    }


    @Override
    public List<Account> accounts() {
        final Long userId = loggedInUserHolder.getLoggedInUserId();
        return accountDao.findByUser(userId).stream()
                .map(this::mapToAccount)
                .collect(Collectors.toList());
    }

    @Override
    public BigDecimal sum() {
        final Long userId = loggedInUserHolder.getLoggedInUserId();
        return accountDao.findSumByUser(userId);
    }

    @Override
    public Account registerNormal() {
        final AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountType(AccountType.NORMAL);
        accountEntity.setBalance(BigDecimal.valueOf(100));
        historyService.registerOperation(OperationType.TRANSFER_INCOMING, BigDecimal.valueOf(100));

        return buildAccountBasedOn(accountEntity);
    }

    @Override
    public Account registerLoan(BigDecimal amount) {
        final AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountType(AccountType.LOAN);
        accountEntity.setOwes(amount);
        accountEntity.setBalance(amount);
        historyService.registerOperation(OperationType.LOAN_ACTIVATION, amount);

        return buildAccountBasedOn(accountEntity);
    }

    @Override
    public Account registerSavings() {
        final AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountType(AccountType.SAVINGS);

        return buildAccountBasedOn(accountEntity);
    }

    private Account buildAccountBasedOn(AccountEntity accountEntity) {
        final Long loggedInUserId = loggedInUserHolder.getLoggedInUserId();
        final UserEntity loggedInUser = userDao.find(loggedInUserId);
        final Integer accoutsAmount = Optional.ofNullable(loggedInUser.getAccounts())
                .map(List::size)
                .orElse(0);

        final String uuidBase = loggedInUser.getPesel() + accoutsAmount;

        final String accountUuid = UUID.nameUUIDFromBytes(uuidBase.getBytes()).toString();
        accountEntity.setUuid(accountUuid);
        accountEntity.setOwner(loggedInUser);

        final AccountEntity savedAccount = this.accountDao.save(accountEntity);
        return mapToAccount(savedAccount);
    }

    private Account mapToAccount(AccountEntity accountEntity) {
        return Account.builder()
                .balance(accountEntity.getBalance())
                .uuid(accountEntity.getUuid())
                .type(accountEntity.getAccountType())
                .build();
    }

    @Override
    public void withdraw(String uuid, BigDecimal amount) {
        final AccountEntity acct = accountDao.findByUserAndAccount(loggedInUserHolder.getLoggedInUserId(), uuid);
        acct.setBalance(acct.getBalance().subtract(amount));
        historyService.registerOperation(OperationType.WITHDRAW, amount);

    }

    @Override
    public void deposit(String uuid, BigDecimal amount) {
        final AccountEntity acct = accountDao.findByUserAndAccount(loggedInUserHolder.getLoggedInUserId(), uuid);
        acct.setBalance(acct.getBalance().add(amount));
        historyService.registerOperation(OperationType.DEPOSIT, amount);
    }

    @Override
    public void payLoan(String uuid) {
        final AccountEntity acct = accountDao.findByUserAndAccount(loggedInUserHolder.getLoggedInUserId(), uuid);
        acct.setAccountType(AccountType.NORMAL);
        payLoan(acct, acct.getOwes());
    }

    @Override
    public void payLoan(String uuid, BigDecimal amount) {
        final AccountEntity acct = accountDao.findByUserAndAccount(loggedInUserHolder.getLoggedInUserId(), uuid);
        payLoan(acct, amount);
    }

    private void payLoan(AccountEntity acct, BigDecimal amount) {
        historyService.registerOperation(OperationType.LOAN_REPAYMENT, amount);

        acct.setBalance(acct.getBalance().subtract(amount));
        acct.setOwes(acct.getOwes().subtract(amount));
    }

    @Override
    public void transfer(String sender, String receiver, BigDecimal amount) {
        final AccountEntity senderAcct = accountDao.findByUserAndAccount(loggedInUserHolder.getLoggedInUserId(), sender);
        final BigDecimal senderAfterOperation = senderAcct.getBalance().subtract(amount);
        senderAcct.setBalance(senderAfterOperation);
        historyService.registerOperation(OperationType.TRANSFER_OUTGOING, amount);

        final AccountEntity receiverAcct = accountDao.findByAccount(receiver);
        final BigDecimal receiverAfterOperation = receiverAcct.getBalance().add(amount);
        receiverAcct.setBalance(receiverAfterOperation);
        historyService.asUser(receiverAcct.getOwner()).registerOperation(OperationType.TRANSFER_OUTGOING, amount);
    }
}
