package pl.sda.hibernate.service.impl;

import pl.sda.hibernate.dao.AccountDao;

public class PlannedEventsServiceImpl implements pl.sda.hibernate.service.PlannedEventsService {
    private final AccountDao accountDao;


    public PlannedEventsServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void calculate() {
        accountDao.incrementAllSavings();
        accountDao.decrementAllLoans();
        accountDao.updateLoanToNormalWhereOwesZero();
    }
}
