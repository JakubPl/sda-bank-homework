package pl.sda.hibernate.service;

import pl.sda.hibernate.dto.HistoryRecord;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.entity.enums.OperationType;
import pl.sda.hibernate.service.impl.HistoryServiceImpl;

import java.math.BigDecimal;
import java.util.List;

public interface HistoryService {
    void registerOperation(OperationType operationType, BigDecimal balanceChange);

    List<HistoryRecord> listUserActions();

    List<HistoryRecord> listUserTransactions();

    HistoryServiceImpl asUser(UserEntity userEntity);
}
