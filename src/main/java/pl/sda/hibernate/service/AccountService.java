package pl.sda.hibernate.service;

import pl.sda.hibernate.dto.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {
    List<Account> accounts();

    BigDecimal sum();

    Account registerNormal();

    Account registerLoan(BigDecimal amount);

    Account registerSavings();

    void withdraw(String uuid, BigDecimal amount);

    void deposit(String uuid, BigDecimal amount);

    void payLoan(String uuid);

    void payLoan(String uuid, BigDecimal amount);

    void transfer(String sender, String receiver, BigDecimal amount);
}
