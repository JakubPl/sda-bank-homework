package pl.sda.hibernate.service;

import pl.sda.hibernate.entity.UserEntity;

public class LoggedInUserHolder {

    public static final LoggedInUserHolder NO_USER = new LoggedInUserHolder();
    private final Long loggedInUserId;
    private final String performerName;


    private LoggedInUserHolder() {
        this.loggedInUserId = null;
        this.performerName = "LOGGED OUT";
    }

    private LoggedInUserHolder(Long loggedInUserId, String performerName) {
        this.loggedInUserId = loggedInUserId;
        this.performerName = performerName;
    }

    public Long getLoggedInUserId() {
        return loggedInUserId;
    }



    public static LoggedInUserHolder actOnBehalfOf(long userId) {
        return new LoggedInUserHolder(userId, "SYSTEM");
    }

    public static LoggedInUserHolder asUser(UserEntity userEntity) {
        return new LoggedInUserHolder(userEntity.getId(), userEntity.getName() + " " + userEntity.getSurname());
    }
}
