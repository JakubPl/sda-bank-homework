package pl.sda.hibernate.dto;

import pl.sda.hibernate.entity.enums.AccountType;

import java.math.BigDecimal;

public class Account {
    private String uuid;
    private BigDecimal balance;
    private AccountType type;


    private Account(AccountBuilder builder) {
        this.uuid = builder.uuid;
        this.balance = builder.balance;
        this.type = builder.type;
    }

    public String getUuid() {
        return uuid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public AccountType getType() {
        return type;
    }

    public static AccountBuilder builder() {
        return new AccountBuilder();
    }

    public static final class AccountBuilder {
        private String uuid;
        private AccountType type;
        private BigDecimal balance;


        private AccountBuilder() {
        }

        public AccountBuilder uuid(String uuid) {
            this.uuid = uuid;
            return this;
        }

        public AccountBuilder type(AccountType type) {
            this.type = type;
            return this;
        }

        public AccountBuilder balance(BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
