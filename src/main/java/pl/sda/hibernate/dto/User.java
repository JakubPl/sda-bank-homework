package pl.sda.hibernate.dto;

public class User {
    private String pesel;
    private String name;
    private String surname;
    private String password;
    private String repeatPassword;

    private User(UserBuilder userBuilder) {
        this.pesel = userBuilder.pesel;
        this.surname = userBuilder.surname;
        this.repeatPassword = userBuilder.repeatPassword;
        this.name = userBuilder.name;
        this.password = userBuilder.password;
    }

    public String getPesel() {
        return pesel;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public static final class UserBuilder {
        private String pesel;
        private String name;
        private String surname;
        private String password;
        private String repeatPassword;

        private UserBuilder() {
        }

        public UserBuilder pesel(String pesel) {
            this.pesel = pesel;
            return this;
        }

        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public UserBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder repeatPassword(String repeatPassword) {
            this.repeatPassword = repeatPassword;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
