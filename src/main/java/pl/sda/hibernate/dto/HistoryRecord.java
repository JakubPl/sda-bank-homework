package pl.sda.hibernate.dto;

import pl.sda.hibernate.entity.enums.OperationType;

import java.math.BigDecimal;

public class HistoryRecord {
    private OperationType operationType;
    private BigDecimal amount;

    public HistoryRecord(OperationType operationType, BigDecimal amount) {
        this.operationType = operationType;
        this.amount = amount;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
