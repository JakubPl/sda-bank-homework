package pl.sda.hibernate;

import org.hibernate.SessionFactory;
import pl.sda.hibernate.dao.AccountDao;
import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.dao.impl.AccountDaoImpl;
import pl.sda.hibernate.dao.impl.HistoryDaoImpl;
import pl.sda.hibernate.dao.impl.UserDaoImpl;
import pl.sda.hibernate.dto.Account;
import pl.sda.hibernate.dto.User;
import pl.sda.hibernate.service.AccountService;
import pl.sda.hibernate.service.HistoryService;
import pl.sda.hibernate.service.LoggedInUserHolder;
import pl.sda.hibernate.service.impl.*;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String LOGGED_IN_ACTIONS_MENU = "1. Wyloguj sie\n2. Przelew\n3. Wyplac\n4. Wplac \n5. Historia operacji\n6. Historia przelewow\n7. Kredyt\n8. Konto oszczednosciowe\n9. Wyswietl rachunki";
    private static final String NOT_LOGGED_IN_ACTIONS_MENU = "1. Zaloz konto\n2. Zaloguj sie\n3. Nastepny miesiac\n4. Zakoncz";

    public static void main(String[] args) {
        LoggedInUserHolder currentUser = LoggedInUserHolder.NO_USER;

        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

        Scanner scanner = new Scanner(System.in);
        boolean running = true;
        while (running) {
            final String choice;
            final EntityManager entityManager = sessionFactory.createEntityManager();
            if (currentUser == LoggedInUserHolder.NO_USER) {
                //uzytkownik niezalogowany
                System.out.println(NOT_LOGGED_IN_ACTIONS_MENU);
                choice = scanner.nextLine();
                if ("1".equals(choice)) {
                    handleUserRegistration(sessionFactory, scanner);
                } else if ("2".equals(choice)) {
                    currentUser = handleLogin(currentUser, sessionFactory, scanner);
                } else if ("3".equals(choice)) {
                    nextMonth(entityManager);
                } else if ("4".equals(choice)) {

                    running = false;
                }
            } else {
                //uzytkownik zalogowany
                System.out.println(LOGGED_IN_ACTIONS_MENU);
                choice = scanner.nextLine();
                if ("1".equals(choice)) {
                    //wyloguj
                    currentUser = LoggedInUserHolder.NO_USER;
                } else if ("2".equals(choice)) {
                    //przelew
                    transfer(currentUser, scanner, entityManager);
                } else if ("3".equals(choice)) {
                    //wyplata
                    withdraw(currentUser, scanner, entityManager);
                }  else if ("4".equals(choice)) {
                    //wplata
                    deposit(currentUser, scanner, entityManager);
                }  else if ("5".equals(choice)) {
                    //historia operacji
                    operationHistory(currentUser, entityManager);
                }  else if ("6".equals(choice)) {
                    //historia przelewow
                    transactionHistory(currentUser, entityManager);
                } else if ("7".equals(choice)) {
                    //otwarcie kredytu
                    openLoan(currentUser, entityManager, scanner);
                }  else if ("8".equals(choice)) {
                    //otwarcie konta oszczednosciowego
                    openSavingsAccount(currentUser, entityManager);
                } else if ("9".equals(choice)) {
                    //listowanie
                    listAccounts(currentUser, entityManager);
                }
            }
            entityManager.close();
        }
        HibernateUtils.shutdown();
    }

    private static void nextMonth(EntityManager entityManager) {
        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final PlannedEventsServiceImpl plannedEventsService = new PlannedEventsServiceImpl(accountDao);
        entityManager.getTransaction().begin();
        plannedEventsService.calculate();
        entityManager.getTransaction().commit();
    }

    private static void transfer(LoggedInUserHolder currentUser, Scanner scanner, EntityManager entityManager) {
        System.out.println("Podaj uuid konta do obciazenia");
        final String sender = scanner.nextLine();
        System.out.println("Podaj uuid konta do zasilenia");
        final String receiver = scanner.nextLine();
        System.out.println("Podaj ilosc srodkow do przelania");
        final BigDecimal amount = new BigDecimal(scanner.nextLine());

        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);

        entityManager.getTransaction().begin();
        accountService.transfer(sender, receiver, amount);
        entityManager.getTransaction().commit();
    }

    private static void withdraw(LoggedInUserHolder currentUser, Scanner scanner, EntityManager entityManager) {
        System.out.println("Podaj uuid konta do wyplaty");
        final String uuid = scanner.nextLine();
        System.out.println("Podaj ilosc srodkow do wyplaty");
        final BigDecimal amount = new BigDecimal(scanner.nextLine());

        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);

        entityManager.getTransaction().begin();
        accountService.withdraw(uuid, amount);
        entityManager.getTransaction().commit();
    }

    private static void deposit(LoggedInUserHolder currentUser, Scanner scanner, EntityManager entityManager) {
        System.out.println("Podaj uuid konta do wplaty");
        final String uuid = scanner.nextLine();
        System.out.println("Podaj ilosc srodkow do wplaty");
        final BigDecimal amount = new BigDecimal(scanner.nextLine());

        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);

        entityManager.getTransaction().begin();
        accountService.deposit(uuid, amount);
        entityManager.getTransaction().commit();
    }

    private static void operationHistory(LoggedInUserHolder currentUser, EntityManager entityManager) {
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);

        historyService.listUserActions()
            .forEach(action -> System.out.printf("%s %s%n", action.getOperationType(), action.getAmount()));
    }

    private static void transactionHistory(LoggedInUserHolder currentUser, EntityManager entityManager) {
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);

        historyService.listUserTransactions()
                .forEach(action -> System.out.printf("%s %s%n", action.getOperationType(), action.getAmount()));
    }

    private static void openLoan(LoggedInUserHolder currentUser, EntityManager entityManager, Scanner scanner) {
        System.out.println("Podaj kwote kredytu");
        final BigDecimal loanValue = new BigDecimal(scanner.nextLine());
        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);
        entityManager.getTransaction().begin();
        final Account loanAcct = accountService.registerLoan(loanValue);
        entityManager.getTransaction().commit();
        System.out.println("Uuid konta z kredytem to: " + loanAcct.getUuid());
    }

    private static void openSavingsAccount(LoggedInUserHolder currentUser, EntityManager entityManager) {
        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);

        entityManager.getTransaction().begin();
        final Account savAcct = accountService.registerSavings();
        System.out.println("Uuid konta oszczednosciowego to: " + savAcct.getUuid());
        entityManager.getTransaction().commit();
    }

    private static void listAccounts(LoggedInUserHolder currentUser, EntityManager entityManager) {
        final AccountDao accountDao = new AccountDaoImpl(entityManager);
        final UserDao userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(currentUser, new HistoryDaoImpl(entityManager), userDao);
        final AccountService accountService = new AccountServiceImpl(currentUser, historyService, userDao, accountDao);

        final List<Account> accounts = accountService.accounts();
        final BigDecimal acctsSum = accountService.sum();
        accounts.forEach(acct -> System.out.printf("%s %s %s%n", acct.getUuid(), acct.getType().name(), acct.getBalance()));
        System.out.println("Suma srodkow na " + accounts.size() + " rachunkach wynosi " + acctsSum);
    }

    private static void handleUserRegistration(SessionFactory sessionFactory, Scanner scanner) {
        System.out.println("Podaj imie");
        final String name = scanner.nextLine();
        System.out.println("Podaj nazwisko");
        final String surname = scanner.nextLine();
        System.out.println("Podaj pesel (11 znakow)");
        final String pesel = scanner.nextLine();
        System.out.println("Podaj haslo (min. 8 znakow)");
        final String password = scanner.nextLine();
        System.out.println("Powtorz haslo");
        final String repeatPassword = scanner.nextLine();

        final User userDto = User.builder()
                .name(name)
                .surname(surname)
                .pesel(pesel)
                .password(password)
                .repeatPassword(repeatPassword)
                .build();
        final EntityManager entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final HistoryService historyService = new HistoryServiceImpl(LoggedInUserHolder.NO_USER, new HistoryDaoImpl(entityManager), userDao);
        new UserServiceImpl(historyService, userDao, new AccountDaoImpl(entityManager)).register(userDto);
        entityManager.getTransaction().commit();
    }

    private static LoggedInUserHolder handleLogin(LoggedInUserHolder currentUser, SessionFactory sessionFactory, Scanner scanner) {
        System.out.println("Podaj pesel");
        final String pesel = scanner.nextLine();
        System.out.println("Podaj haslo");
        final String password = scanner.nextLine();
        final EntityManager entityManager = sessionFactory.createEntityManager();

        final UserDaoImpl userDao = new UserDaoImpl(entityManager);
        final CurrentUserContextServiceImpl currentUserContextService = new CurrentUserContextServiceImpl(userDao, currentUser);
        currentUser = currentUserContextService.login(pesel, password);
        return currentUser;
    }
}
