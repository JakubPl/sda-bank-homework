package pl.sda.hibernate.entity;

import pl.sda.hibernate.entity.enums.OperationType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class HistoryEntity extends BaseEntity {
    @Column(name = "chng")
    private BigDecimal change;
    @Enumerated(EnumType.STRING)
    private OperationType operationType;
    private LocalDateTime created;
    @ManyToOne
    private UserEntity owner;

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal amount) {
        this.change = amount;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }
}
