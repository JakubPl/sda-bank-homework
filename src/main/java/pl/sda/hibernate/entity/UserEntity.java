package pl.sda.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class UserEntity extends BaseEntity {
    private String name;
    private String surname;
    @Column(unique = true)
    private String pesel;
    private String password;

    @OneToMany(mappedBy = "owner")
    private List<AccountEntity> accounts;

    @OneToMany(mappedBy = "owner")
    private List<HistoryEntity> historyRecords;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<AccountEntity> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountEntity> accounts) {
        this.accounts = accounts;
    }

    public List<HistoryEntity> getHistoryRecords() {
        return historyRecords;
    }

    public void setHistoryRecords(List<HistoryEntity> historyRecords) {
        this.historyRecords = historyRecords;
    }
}
