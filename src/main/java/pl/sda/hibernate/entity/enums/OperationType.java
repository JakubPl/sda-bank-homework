package pl.sda.hibernate.entity.enums;

import java.util.*;

public enum OperationType {
    DEPOSIT, WITHDRAW, TRANSFER_INCOMING, TRANSFER_OUTGOING, LOAN_REPAYMENT, SAVINGS, LOAN_ACTIVATION;

    public static final Set<OperationType> TRANSFERS;

    static {
        final List<OperationType> transferOperationTypesList = Arrays.asList(OperationType.TRANSFER_INCOMING, OperationType.TRANSFER_OUTGOING);
        TRANSFERS = Collections.unmodifiableSet(new HashSet<>(transferOperationTypesList));
    }
}
