package pl.sda.hibernate.entity.enums;

public enum AccountType {
    SAVINGS, LOAN, NORMAL
}
