package pl.sda.hibernate.entity;

import pl.sda.hibernate.entity.enums.AccountType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class AccountEntity extends BaseEntity {
    private String uuid;
    @Column(nullable = false)
    private BigDecimal balance = BigDecimal.ZERO;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountType accountType;
    private BigDecimal owes;

    @ManyToOne
    private UserEntity owner;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }

    public BigDecimal getOwes() {
        return owes;
    }

    public void setOwes(BigDecimal owes) {
        this.owes = owes;
    }
}
