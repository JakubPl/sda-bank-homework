package pl.sda.hibernate.dao;

import pl.sda.hibernate.entity.UserEntity;

public interface UserDao extends BaseDao<UserEntity> {
    UserEntity findByPeselAndPassword(String pesel, String password);
}
