package pl.sda.hibernate.dao.impl;

import pl.sda.hibernate.dao.UserDao;
import pl.sda.hibernate.entity.UserEntity;

import javax.persistence.EntityManager;

public class UserDaoImpl extends BaseDaoImpl<UserEntity> implements UserDao {
    private final EntityManager entityManager;

    public UserDaoImpl(EntityManager entityManager) {
        super(entityManager, UserEntity.class);
        this.entityManager = entityManager;
    }

    @Override
    public UserEntity findByPeselAndPassword(String pesel, String password) {
        return entityManager.createQuery("FROM UserEntity user WHERE user.pesel = :pesel AND user.password = :password", UserEntity.class)
                .setParameter("pesel", pesel)
                .setParameter("password", password)
                .getSingleResult();
    }
}
