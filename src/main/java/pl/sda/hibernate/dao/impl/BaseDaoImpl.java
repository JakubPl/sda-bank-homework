package pl.sda.hibernate.dao.impl;

import pl.sda.hibernate.dao.BaseDao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public abstract class BaseDaoImpl<ENTITY> implements BaseDao<ENTITY> {
    private EntityManager entityManager;
    // to jest potrzebne tylko, zeby generyczne/bazowe dao dzialalo (da sie to uzyskac tez przez refleksje, ale nie czas na hacki :))
    private final Class<ENTITY> entityClass;

    public BaseDaoImpl(EntityManager entityManager, Class<ENTITY> entityClass) {
        this.entityManager = entityManager;
        this.entityClass = entityClass;
    }

    @Override
    public List<ENTITY> findAll() {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<ENTITY> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        criteriaQuery.from(entityClass);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public ENTITY find(long id) {
        return entityManager.find(entityClass, id);
    }

    @Override
    public void delete(ENTITY entity) {
        entityManager.remove(entity);
    }

    @Override
    public ENTITY save(ENTITY newEntity) {
        entityManager.persist(newEntity);
        return newEntity;
    }

    @Override
    public ENTITY update(ENTITY toUpdate) {
        return entityManager.merge(toUpdate);
    }
}
