package pl.sda.hibernate.dao.impl;

import pl.sda.hibernate.dao.AccountDao;
import pl.sda.hibernate.entity.AccountEntity;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

public class AccountDaoImpl extends BaseDaoImpl<AccountEntity> implements AccountDao {
    private final EntityManager entityManager;

    public AccountDaoImpl(EntityManager entityManager) {
        super(entityManager, AccountEntity.class);
        this.entityManager = entityManager;
    }

    @Override
    public List<AccountEntity> findByUser(long userId) {
        return entityManager.createQuery("FROM AccountEntity acct WHERE acct.owner.id = :ownerId", AccountEntity.class)
                .setParameter("ownerId", userId)
                .getResultList();
    }

    @Override
    public AccountEntity findByUserAndAccount(long userId, String acctUuid) {
        return entityManager.createQuery("FROM AccountEntity acct WHERE acct.owner.id = :ownerId AND acct.uuid = :acct", AccountEntity.class)
                .setParameter("ownerId", userId)
                .setParameter("acct", acctUuid)
                .getSingleResult();
    }

    @Override
    public BigDecimal findSumByUser(Long userId) {
        return entityManager.createQuery("SELECT SUM(acct.balance) FROM AccountEntity acct WHERE acct.owner.id = :ownerId", BigDecimal.class)
                .setParameter("ownerId", userId)
                .getSingleResult();
    }

    @Override
    public void incrementAllSavings() {
        entityManager.createQuery("UPDATE AccountEntity acct SET acct.balance = acct.balance + 100 WHERE acct.accountType = pl.sda.hibernate.entity.enums.AccountType.SAVINGS")
                .executeUpdate();
    }

    @Override
    public void decrementAllLoans() {
        entityManager.createQuery("UPDATE AccountEntity acct SET acct.balance = acct.balance - 100, acct.owes = acct.owes - 100 WHERE acct.accountType = pl.sda.hibernate.entity.enums.AccountType.LOAN AND acct.owes > 0")
                .executeUpdate();
    }

    @Override
    public AccountEntity findByAccount(String acctUuid) {
        return entityManager.createQuery("FROM AccountEntity acct WHERE acct.uuid = :acct", AccountEntity.class)
                .setParameter("acct", acctUuid)
                .getSingleResult();
    }

    @Override
    public void updateLoanToNormalWhereOwesZero() {
        entityManager.createQuery("UPDATE AccountEntity acct SET acct.accountType = pl.sda.hibernate.entity.enums.AccountType.NORMAL WHERE acct.accountType = pl.sda.hibernate.entity.enums.AccountType.LOAN AND acct.owes = 0")
                .executeUpdate();
    }
}
