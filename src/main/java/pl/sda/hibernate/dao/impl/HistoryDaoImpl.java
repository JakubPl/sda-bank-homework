package pl.sda.hibernate.dao.impl;

import pl.sda.hibernate.dao.HistoryDao;
import pl.sda.hibernate.entity.HistoryEntity;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.entity.enums.OperationType;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Set;

public class HistoryDaoImpl extends BaseDaoImpl<HistoryEntity> implements HistoryDao {
    private final EntityManager entityManager;

    public HistoryDaoImpl(EntityManager entityManager) {
        super(entityManager, HistoryEntity.class);
        this.entityManager = entityManager;
    }

    @Override
    public List<HistoryEntity> findByUser(long loggedInUserId, Set<OperationType> operationTypes) {
        return entityManager.createQuery("FROM HistoryEntity history JOIN FETCH history.owner owner WHERE owner.id = :ownerId AND history.operationType IN :operationTypes ORDER BY history.created", HistoryEntity.class)
                .setParameter("ownerId", loggedInUserId)
                .setParameter("operationTypes", operationTypes)
                .getResultList();
    }
}
