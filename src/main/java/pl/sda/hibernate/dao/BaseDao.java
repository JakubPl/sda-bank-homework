package pl.sda.hibernate.dao;

import java.util.List;

public interface BaseDao<ENTITY> {
    List<ENTITY> findAll();
    ENTITY find(long id);
    void delete(ENTITY entity);
    ENTITY save(ENTITY newEntity);
    ENTITY update(ENTITY newEntity);
}
