package pl.sda.hibernate.dao;

import pl.sda.hibernate.entity.AccountEntity;
import pl.sda.hibernate.entity.HistoryEntity;
import pl.sda.hibernate.entity.UserEntity;
import pl.sda.hibernate.entity.enums.OperationType;

import java.util.List;
import java.util.Set;

public interface HistoryDao extends BaseDao<HistoryEntity> {
    List<HistoryEntity> findByUser(long loggedInUser, Set<OperationType> operationTypes);
}
