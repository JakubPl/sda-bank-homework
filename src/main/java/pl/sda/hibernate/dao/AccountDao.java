package pl.sda.hibernate.dao;

import pl.sda.hibernate.entity.AccountEntity;
import pl.sda.hibernate.entity.UserEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public interface AccountDao extends BaseDao<AccountEntity> {
    List<AccountEntity> findByUser(long id);
    AccountEntity findByUserAndAccount(long userId, String acctUuid);
    BigDecimal findSumByUser(Long userId);

    void incrementAllSavings();

    void decrementAllLoans();

    AccountEntity findByAccount(String uuid);

    void updateLoanToNormalWhereOwesZero();
}
